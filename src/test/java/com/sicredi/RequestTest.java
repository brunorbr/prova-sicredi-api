package com.sicredi;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.List;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class RequestTest {
    public static RequestSpecification requestSpec;

    @BeforeAll
    public static void setSpecification(){
        requestSpec = new RequestSpecBuilder()
            .setBaseUri("https://viacep.com.br/ws/")
            .setContentType(ContentType.JSON)
            .build();
    }

    @Test
    public void CEPValido_ValidaResposta(){
        given().
                spec(requestSpec).
        when().
                get("91130230/json").
        then().
                assertThat().statusCode(200).
                and().
                body("cep", equalTo("91130-230")).
                and().
                body("logradouro", equalTo("Rua Lindolfo Henke")).
                and().
                body("bairro", equalTo("Sarandi")).
                and().
                body("localidade", equalTo("Porto Alegre")).
                and().
                body("uf", equalTo("RS")).
                and().
                body("ibge", equalTo("4314902"));
    }

    @Test
    public void ConsultaCEPInexistente_ValidaFalha(){
        given().
                spec(requestSpec).
        when().
                get("92500000/json").
        then().
                assertThat().statusCode(404).
                and().
                body("erro", equalTo(true));
    }

    @Test
    public void ConsultaCEPInvalido_ValidaFalha(){
        given().
                spec(requestSpec).
        when().
                get("ABC452-000/json").
        then().
                assertThat().statusCode(400).
                and().body("erro", equalTo("CEP Inválido"));
    }

    @Test
    public void ConsultaBairro_ValidaQuantidadeRuas(){
        List list =
                given().
                        spec(requestSpec).
                when().
                        get("RS/Gravatai/Barroso/json/").
                then().
                        assertThat().statusCode(200).
                        extract().as(List.class);
        Assertions.assertEquals(list.size(), 2);
    }
}
